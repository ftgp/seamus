/* global requirejs */

/**
 * RequireJS config file
 */

requirejs.config({
    paths: {
		diffractor: "node_modules/diffractor/dist/diffractor.min"
	},
    shim: {
		"diffractor": {deps: ["jquery", "klassified"]},
	},
    enforceDefine: false
});
