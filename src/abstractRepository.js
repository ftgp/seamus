define([
	"klassified"
], function(klassified) {
	return klassified.object.subclass((that, my) => {

		that.get = function(id) {
			my.subclassResponsibility();
		};

		that.post = function(object) {
			my.subclassResponsibility();
		};

		that.put = function(object) {
			my.subclassResponsibility();
		};

		that.delete = function() {
			my.subclassResponsibility();
		};
	});
});
