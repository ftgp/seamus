define([
	"klassified"
], function(klassified) {
	const model = klassified.object.abstractSubclass((that, my) => {
		/**
		 * Default implementation. Returns all properties on `my` serialized.
		 * @return {object} Literal representation of the model
		 */
		that.serialized = function() {
			return my.serializeProperties(my.serializedProperties());
		};

		/**
		 * Default `toString` implementation for models.
		 * @return {string} string representation of the instance.
		 */
		that.toString = function() {
			return JSON.stringify(that.serialized());
		};

		that.isModel = function() {
			return true;
		};

		my.serializedProperties = function() {
			let result = {};
			Object.assign(result, my);
			return result;
		};

		my.serializeProperties = function(schema) {
			var properties = {};

			Object.keys(schema).forEach(function(key) {
				var property = schema[key];
				if (property === undefined || property === null) {
					properties[key] = property;
					return;
				}

				// Exclude protected methods
				if (typeof property === "function") {
					return;
				}

				if (property.constructor === Array) {
					properties[key] = property.map(my.serializeProperty);
					return;
				}

				if (!property.isModel && typeof property === "object") {
					properties[key] = my.serializeProperties(property);
					return;
				}

				properties[key] = my.serializeProperty(property);
			});

			return properties;
		};

		my.serializeProperty = function(property) {
			if (property === null || property === undefined) {
				return property;
			}

			if (property.isModel && property.isModel()) {
				return property.serialize();
			}
			return property;
		};
	});

	klassified.object.extend((that, my) => {
		that.isModel = function() {
			return false;
		};
	});

	return model;
});
