define([
	"klassified",
	"model"
], function(klassified, model) {
	return klassified.testCase.subclass((that, my) => {
		my.initialize = function() {
			my.super(...arguments);

			my.simpleModel = model.subclass((that, my) => {
				my.initialize = function() {
					my.super(...arguments);
					my.string = "string";
					my.number = 42;
					my.boolean = false;
					my.array = ["string", 42, false];
				};
			});

			klassified.object.extend((that,my) => {
				that.getMy = function() {
					return my;
				};
			});
		};

		my.name = function() {
			return "model";
		};

		my.isModelTest = function() {
			const myModel = model.subclass(() => {});
			expect(myModel().isModel()).toBeTruthy();

			const myObject = klassified.object.subclass(() => {});
			expect(myObject().isModel()).toBeFalsy();
		};

		my.serializedPropertiesTest = function() {
			const object = my.simpleModel();
			const properties = object.getMy().serializedProperties();

			expect(properties.string).toEqual("string");
			expect(properties.number).toEqual(42);
			expect(properties.boolean).toEqual(false);
			expect(properties.array).toEqual(["string", 42, false]);
		};

		// We need to make sure that the serialized properties have all
		// properties as *own* keys for iteration
		my.serializedPropertiesKeysTest = function() {
			const properties = my.simpleModel().getMy().serializedProperties();
			["string", "number", "boolean", "array"].forEach((key) => {
				expect(Object.keys(properties)).toContain(key);
			});
		};

		my.serializedTest = function() {
			const data = my.simpleModel().serialized();
			expect(data).toEqual({
				string: "string",
				number: 42,
				boolean: false,
				array: ["string", 42, false]
			});
		};
	});
});
