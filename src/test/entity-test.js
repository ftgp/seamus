define([
	"klassified",
	"entity",
	"repository"
], function(klassified, entity, repository) {
	return klassified.testCase.subclass((that, my) => {
		my.initialize = function() {
			my.super(...arguments);

			my.person = entity.subclass((that, my) => {
				my.initialize = function({name}) {
					my.super(...arguments);
					my.name = name;
				};
			});

			my.person.class((that, my) => {
				that.path = function() {
					return "people";
				};
			});
		};

		my.name = function() {
			return "entity";
		};

		my.uniqueIdTest = function() {
			const dupont = my.person();
			const dupond = my.person();
			expect(dupont.getId()).not.toEqual(dupond.getId());
		};

		my.uniqueIdIsNonTrivialTest = function() {
			expect(my.person().getId().length > 10).toBeTruthy();
		};

		my.savingNewEntityTest = function() {
			const toto = my.person({name: "toto"});
			spyOn(toto, "insert");
			spyOn(toto, "update");

			toto.save();

			expect(toto.insert).toHaveBeenCalledWith();
			expect(toto.update).not.toHaveBeenCalled();
		};

		my.savingExistingEntityTest = function() {
			const toto = my.person({name: "toto", isNew: false});
			spyOn(toto, "insert");
			spyOn(toto, "update");

			toto.save();

			expect(toto.update).toHaveBeenCalledWith();
			expect(toto.insert).not.toHaveBeenCalled();
		};

		my.defaultRepositoryTest = function() {
			const toto = my.person({name: "toto", isNew: false});
			expect(toto.getClass().getRepository().getClass()).toBe(repository);
		};

		my.defaultIdPropertyTest = function() {
			expect(my.person.idProperty()).toBe("id");
		};

		my.testingStuffTest = function() {
			let user = entity.subclass((that, my) => {

			});

			user.class((that, my) => {
				that.path = function() {
					return "/Agency2/test/AgencyUser/#id";
				};
			});

			user.find("damiend").then((user) => {
				window.alert(user);
			});

			expect(true).toBeFalsy();
		};
	});
});
