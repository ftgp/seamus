define([], function() {
	var files = [
		"test/repository-test",
		"test/model-test",
		"test/entity-test"
	];
	return {
		files: files,
		config: {
			paths: {
				"jquery": "../node_modules/jquery/dist/jquery",
				"klassified": "../node_modules/klassified/dist/klassified",
				"diffractor": "../node_modules/diffractor/dist/diffractor"
			}
		},
		shim: {
			"jquery": {
				exports: "$"
			}
		}
	};
});
