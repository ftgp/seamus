define([
	"./abstractRelationship"
], function(abstractRelationship) {
	let toOneRelationship = abstractRelationship.subclass((that, my) => {
		my.initialize = function({name, id}) {
			my.super(...arguments);
			my.id = id || (name + "Id");
		};
	});

	toOneRelationship.class((that) => {
		that.arity = function() {
			return "1";
		};
	});

	return toOneRelationship;
});
