define([
	"./abstractRelationship"
], function(abstractRelationship) {
	let toManyRelationship = abstractRelationship.subclass((that, my) => {
		my.initialize = function({name, id}) {
			my.super(...arguments);
			my.id = id || (name + "Ids");
		};
	});

	toManyRelationship.class((that) => {
		that.arity = function() {
			return "*";
		};
	});

	return toManyRelationship;
});
