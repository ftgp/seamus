define([
	"./abstractRepository",
	"diffractor"
], function(abstractRepository, diffractor) {
	let repository = abstractRepository.subclass((that, my) => {
		my.initialize = function({path}) {
			my.super(...arguments);
			my.route = diffractor.route({pattern: path});
		};

		that.get = function(id) {
			return my.query("GET", my.route.expand({id}));
		};

		that.post = function(model) {
			return my.query("POST", my.route.expand(), model.serialized());
		};

		that.put = function(model) {
			return my.query("PUT", my.route.expand(), model.serialized());
		};

		that.delete = function(id) {
			return my.query("DELETE", my.route.expand({id}));
		};

		my.query = function(method, url, data) {
			return window.fetch("http://localhost:7006/" + url, {
				method,
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Basic ${window.btoa("damiend:damiend")}`
				},
				body: JSON.stringify(data)
			});
		};
	});

	return repository;
});
