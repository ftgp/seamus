define([
	"klassified"
], function(klassified) {
	let abstractRelationship = klassified.object.abstractSubclass((that, my) => {
		my.initialize = function({name, id, ownerClass, relationshipClass}) {
			my.super(...arguments);
			my.name = name;
			my.id = id;
			my.ownerClass = ownerClass;
			my.relationshipClass = relationshipClass;
		};

		my.get("id");
	});

	abstractRelationship.class((that) => {
		that.arity = function() {
			return that.subclassResponsibility();
		};

		that.withArity = function(arity) {
			return that.allConcreteSubclasses().find((klass) => {
				return klass.arity() === arity;
			});
		};
	});

	return abstractRelationship;
});
