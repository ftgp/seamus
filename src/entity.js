define([
	"./model",
	"./abstractRelationship",
	"./repository",
	"./repository",
	"./toOneRelationship",
	"./toManyRelationship"
], function(model, abstractRelationship, repository) {
	/**
	 * TODO: Add a `refresh`, or `reload` method that would update the instance
	 * from fresh server data.
	 */
	let entity = model.subclass((that, my) => {
		my.initialize = function({id, isNew}) {
			my.super(...arguments);
			my.id = id || uuid();
			my.isNew = isNew !== false;
		};

		that.getId = function() {
			return my.id;
		};

		/**
		 * Return a promise.
		 *
		 * New instances are being "inserted", while other instances are
		 * "updated". This means that using the default repository, a POST
		 * request is performed for new instances, a PUT request for other
		 * instances.
		 */
		that.save = function() {
			if (my.isNew) {
				return that.insert();
			} else {
				return that.update();
			}
		};

		/**
		 * Return a promise
		 */
		that.remove = function() {
			return my.getClass().remove(that);
		};

		/**
		 * Return a promise
		 */
		that.insert = function() {
			return my.getClass().insert(that);
		};

		/**
		 * Return a promise
		 */
		that.update = function() {
			return my.getClass().update(that);
		};

		/**
		 * Method called by the framework upon successful POST request.
		 */
		that.beSaved = function() {
			my.isNew = false;
		};

		/**
		 * Return a promise
		 */
		my.getRepository = function() {
			return that.getClass().getRepository();
		};
	});

	entity.class((that) => {
		let cache = new WeakMap();

		/**
		 * Remove all instances from the cache.  Doing so will result in new
		 * instances being built from fetched repository data, even when an
		 * instance with the same id has already been created and has not been
		 * garbage-collected.
		 */
		that.flushCache = function() {
			cache = new WeakMap();
		};

		/**
		 * Return the `id` property used to identify instances.  Override in
		 * subclasses when a custom id JSON field is being sent by the server.
		 */
		that.idProperty = function() {
			return "id";
		};

		that.path = function() {
			that.subclassResponsibility();
		};

		/**
		 * Return a description of the relationships between the current class
		 * and other model classes.
		 *
		 * Usage example:
		 *
		 *   return {
		 *		dog: { class: dog, arity: '1' },
		 *		companies: { class: company, arity: '*' }
		 *	};
		 */
		that.relationships = function() {
			return {};
		};

		that.makeRelationships = function() {
			let spec = that.relationships();
			return Object.keys(spec).map((name) => {
				let desc = spec[name];
				desc.name = name;
				return abstractRelationship.withArity(desc.arity)(desc);
			});
		};

		that.getRepository = function() {
			return that.getRepositoryClass()({
				path: that.path(),
				model: that
			});
		};

		that.getRepositoryClass = function() {
			return repository;
		};

		that.insert = function(instance) {
			let promise = that.getRepository().post(instance);
			promise.then(() => instance.beSaved());
			return promise;

		};

		that.update = function(instance) {
			return that.getRepository().put(instance);
		};

		that.remove = function(instance) {
			return that.getRepository().delete(instance.getId());
		};

		/**
		 * Return a promise
		 */
		that.findAll = function() {
			that.getRepository()
				.findAll(...arguments)
				.then((data) => {

				})
				.catch((error) => {

				});
		};

		/**
		 * Return a promise
		 */
		that.find = function(id) {
			return that.getRepository().get(id);
		};

		/**
		 * Setup a "to one" relationship between the current class and `klass`.
		 *
		 * `klass` should be a model class.
		 */
		that.toOne = function({klass, propertyName, propertyIdName}) {
			propertyIdName = propertyIdName || propertyName + "Id";
		};

		/**
		 * Setup a "to many" relationship between the current class and `klass`.
		 *
		 * `klass` should be a model class.
		 */
		that.toMany = function({klass, propertyName, propertyIdName}) {
		};
	});

	function uuid() {
		function _p8(s) {
			var p = (Math.random().toString(16) + "000000000").substr(2, 8);
			return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
		}

		return _p8() + _p8(true) + _p8(true) + _p8();
	}

	return entity;
});
